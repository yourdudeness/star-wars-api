import { useLocation } from "react-router-dom";
import img from "./img/not-found.jpg";
import PropTypes from "prop-types";
import styles from "./NotFoundPage.module.css";

const NotFoundPage = () => {
  let location = useLocation();
  return (
    <>
      <img className={styles.img} src={img} alt="" />
      <p className={styles.text}>
        No match for <u>{location.pathname}</u>
      </p>
    </>
  );
};

export default NotFoundPage;
