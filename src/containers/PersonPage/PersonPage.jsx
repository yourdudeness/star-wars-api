import PropTypes from 'prop-types';
import { useEffect, useState } from 'react';

import { withErrorApi } from "@hoc-helpers/withErrorApi";

import { getApiResource } from '@utils/network';
import { API_PERSON } from '@constants/api'
import { getPeopleImg } from '@services/getPeopleData'

import styles from './PersonPage.module.css';


const PersonPage = ({ match, setErrorApi }) => {
    const [personInfo, setPersonInfo] = useState(null);
    const [personName, setPersonName] = useState(null);
    const [personPhoto, setPersonPhoto] = useState(null)

    useEffect(() => {
        (async () => {
            const id = match.params.id;
            const res = await getApiResource(`${API_PERSON}/${id}/`);

            if (res) {
                setPersonInfo([
                    { title: 'Height', data: res.height },
                    { title: 'Mass', data: res.mass },
                    { title: 'Hair Color', data: res.hair_color },
                    { title: 'Skin Color', data: res.skin_color },
                    { title: 'Eye Color', data: res.eye_color },
                    { title: 'Birth Year', data: res.birth_year },
                    { title: 'Gender', data: res.gender },
                ])

                setPersonName(res.name);
                setPersonPhoto(getPeopleImg(id))
                setErrorApi(false);
            } else {
                setErrorApi(true);
            }
        })();
    }, []);


    return (
        <>
            <h1>
                {personName}
            </h1>
            <img src={personPhoto} alt={personName} />

            {personInfo && (
                <ul>
                    {personInfo.map(({ title, data }) => (
                        data && (
                            <li key={title}>
                                <span>
                                    {title}: {data}
                                </span>
                            </li>
                        )
                    ))}
                </ul>
            )}
        </>
    )
}

PersonPage.propTypes = {
    setErrorApi: PropTypes.func,
    match: PropTypes.object,
}


export default withErrorApi(PersonPage);